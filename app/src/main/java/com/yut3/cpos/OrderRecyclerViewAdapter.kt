package com.yut3.cpos

import android.app.Application
import android.content.Context
import android.graphics.Color
import android.graphics.Point
import android.util.TypedValue
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders


import com.yut3.cpos.OrderFragment.OnListFragmentInteractionListener
import com.yut3.cpos.data.model.OrderItem
import com.yut3.cpos.data.model.Product
import com.yut3.cpos.dummy.DummyContent.DummyItem

import kotlinx.android.synthetic.main.fragment_order.view.*

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class OrderRecyclerViewAdapter(
    private val mContext: Context,
    private val mApplication: Application,
    private val mValues: List<OrderItem>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<OrderRecyclerViewAdapter.ViewHolder>() {
//    private var mProductList: MutableList<DummyItem> = mValues.toMutableList()
    private var mOrderList: MutableList<OrderItem> = mValues.toMutableList()

    private lateinit var mCPosSharedViewModel: CPosSharedViewModel

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as OrderItem
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
        mCPosSharedViewModel = ViewModelProviders.of(mContext as FragmentActivity, ViewModelProvider.AndroidViewModelFactory(mApplication)).get(CPosSharedViewModel::class.java)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_order, parent, false)
        val display = (mContext.getSystemService(Context.WINDOW_SERVICE)!! as WindowManager).defaultDisplay

        val point = Point()
        display.getSize(point)
        // 640: product fragment width
        // 8 + 8 item left and right margin
        // 16 + 16 RecyclerView left and right margin
        view.layoutParams.width = (point.x - TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (640 + 8 + 8 + 16 + 16).toFloat(), mContext.resources.displayMetrics)).toInt()
        val itemWidth = view.layoutParams.width
        val pxWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (15 + 60 + 10 + 8 + 8).toFloat(), mContext.resources.displayMetrics)
        val productTextView = view.findViewById<LinearLayout>(R.id.productTextContent)
        productTextView.layoutParams.width = (itemWidth - pxWidth).toInt()

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mOrderList[position]
        holder.mProductName.text = "Name: ${item.name}"
        holder.mProductCount.text = "Count: ${item.count}"
        holder.mProductPrice.text = "Price: ${item.price}"

        holder.mProductImageView.setImageResource(R.mipmap.ic_launcher)

        holder.mProductColorbarTextView.setBackgroundColor(Color.GREEN)
        holder.mProductColorbarTextView2.setBackgroundColor(Color.GREEN)

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mOrderList.size

    fun removeAt(position: Int) {
        mCPosSharedViewModel.removeOrderProductById(mOrderList[position].name.toInt())
        updateDataSet()
    }

    fun updateDataSet() {
        mOrderList = mCPosSharedViewModel.getOrderItemList()!!
        notifyDataSetChanged()
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mProductColorbarTextView: TextView = mView.productColorbarTextView
        val mProductColorbarTextView2: TextView = mView.productColorbarTextView2
        val mProductImageView: ImageView = mView.productImageView
        val mProductName: TextView = mView.productName
        val mProductCount: TextView = mView.productCount
        val mProductPrice: TextView = mView.productPrice



        override fun toString(): String {
            return super.toString() + " '" + mProductName.text + "'"
        }
    }
}
