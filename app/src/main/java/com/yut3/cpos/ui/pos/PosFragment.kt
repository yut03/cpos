package com.yut3.cpos.ui.pos

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yut3.cpos.R

class PosFragment : Fragment() {

    companion object {
        fun newInstance() = PosFragment()
    }

    private lateinit var viewModel: PosViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.pos_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PosViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
