package com.yut3.cpos

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.yut3.cpos.data.model.OrderItem
import com.yut3.cpos.data.model.Product

class CPosSharedViewModel(application: Application) : AndroidViewModel(application) {
    val orderProductList = MutableLiveData<MutableList<Product>>()
    val orderItemList = MutableLiveData<MutableList<OrderItem>>()

    fun addOrderProduct(product: Product) {
        orderProductList.value!!.add(product)
        updateOrderItemListByOrderProductList()
    }

    fun removeOrderProductById(id: Int) {
        val index = orderProductList.value!!.indexOfFirst { it.id == id }
        orderProductList.value!!.removeAt(index)
        updateOrderItemListByOrderProductList()
    }

    fun setOrderProductList(list: MutableList<Product>) {
        orderProductList.postValue(list)
        updateOrderItemListByOrderProductList()
    }

    fun getOrderProductList(): MutableList<Product>? {
        return orderProductList.value
    }

    fun setOrderItemList(list: MutableList<OrderItem>) {
        orderItemList.postValue(list)
    }

    fun getOrderItemList(): MutableList<OrderItem>? {
        return orderItemList.value
    }

    fun updateOrderItemListByOrderProductList() {
        orderItemList.value!!.clear()
        for (productCount in 0 until orderProductList.value!!.size) {
            val orderItem = orderItemList.value!!.filter { it.name == orderProductList.value!![productCount].id.toString() }
            if (orderItem.isEmpty()) {
                orderItemList.value!!.add(OrderItem(orderProductList.value!![productCount].id.toString(), orderProductList.value!![productCount].barcode.code, 1, 0.0, 0.0))
            } else {
                val index = orderItemList.value!!.indexOfFirst { it.name == orderProductList.value!![productCount].id.toString() }
                orderItemList.value!![index].count += 1
            }
        }
    }
}