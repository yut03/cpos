package com.yut3.cpos

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Point
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.zxing.BarcodeFormat
import com.google.zxing.ResultPoint
import com.yut3.cpos.data.model.CategoryLevel
import com.yut3.cpos.data.model.Product
import com.yut3.cpos.dummy.DummyContent
import com.yut3.cpos.ui.pos.PosFragment
import com.yut3.cpos.ui.pos.ProductFragment
import com.yut3.cpos.ui.pos.dummy.DummyProduct
import kotlinx.android.synthetic.main.dialog_item_counter.*
import kotlinx.android.synthetic.main.pos_activity.*


import com.google.zxing.client.android.BeepManager
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DecoratedBarcodeView
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import com.yut3.cpos.data.model.Barcode
import com.yut3.cpos.data.model.OrderItem
import java.lang.Exception
import java.util.*
import java.util.concurrent.TimeUnit

class PosActivity : AppCompatActivity(),
    OrderFragment.OnListFragmentInteractionListener,
    ProductFragment.OnListFragmentInteractionListener,
    OrderFragment.OnUpdateOrderListListener{


    private lateinit var mBeepManager: BeepManager
    private lateinit var mBarcodeView: DecoratedBarcodeView
    private var mBarcodeScanLastTime: Long = 0

    private lateinit var mCPosSharedViewModel: CPosSharedViewModel

    private var mBackgroundUpdateUiHandler: Handler? = null

    override fun onUpdateOrderList() {
        mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
            UPDATE_UI_DATE_MESSAGE))
    }

    override fun onListFragmentInteraction(item: Product?) {
        when {
            item!!.id == -1 -> { // previous button
                val productFragment = supportFragmentManager.findFragmentById(R.id.product_list_container) as ProductFragment
                val productList = DummyProduct.createDisplayList(item.categoryLevel.getPreviousLevel())
                productFragment.setProductList(productList)
            }
            item.id == 0 -> { // title button
                val productFragment = supportFragmentManager.findFragmentById(R.id.product_list_container) as ProductFragment
                val productList = DummyProduct.createDisplayList(item.categoryLevel)
                productFragment.setProductList(productList)
            }
            else -> { // product item
//                val inflater = this.layoutInflater
                val dialogView = View.inflate(applicationContext, R.layout.dialog_item_counter,null)
                val dialog = AlertDialog.Builder(this)
                    .setView(dialogView)
                    .setPositiveButton("OK") { _, _ ->
                        // add item to order list
                        val itemCountTextView = dialogView.findViewById<TextView>(R.id.tvItemCount)
                        val itemCount: Int = itemCountTextView.text.toString().toInt()
                        for (i in 0 until itemCount) {
                            mCPosSharedViewModel.addOrderProduct(item)
                        }
                        val orderFragment = supportFragmentManager.findFragmentById(R.id.order_list_container) as OrderFragment
                        orderFragment.updateOrderList()

                        mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                            UPDATE_UI_DATE_MESSAGE))
                    }
                    .create()
                val buttonPlus = dialogView.findViewById<Button>(R.id.btnPlus)

                buttonPlus!!.setOnClickListener {
                    val itemCountTextView = dialogView.findViewById<TextView>(R.id.tvItemCount)
                    var counter = itemCountTextView.text.toString().toInt()
                    counter += 1
                    itemCountTextView.text = counter.toString()
                }

                dialogView.findViewById<Button>(R.id.btnMinus)!!.setOnClickListener {
                    val itemCountTextView = dialogView.findViewById<TextView>(R.id.tvItemCount)
                    var counter = itemCountTextView.text.toString().toInt()
                    if (counter > 0) counter -= 1
                    itemCountTextView.text = counter.toString()
                }
                dialog.show()
            }
        }
    }

    override fun onListFragmentInteraction(item: OrderItem?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pos_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.order_list_container, OrderFragment.newInstance(1))
                .add(R.id.product_list_container, ProductFragment.newInstance(4))
//                .replace(R.id.container, PosFragment.newInstance())
                .commit()
        }

        iv_pos_left.setImageResource(R.mipmap.ic_cpos_launcher)
        iv_pos_right.setImageResource(R.mipmap.ic_launcher)

        openCamera()
        createBarcodeView()

        mCPosSharedViewModel = ViewModelProviders.of(this, ViewModelProvider.AndroidViewModelFactory(application)).get(CPosSharedViewModel::class.java)
        mCPosSharedViewModel.orderProductList.value = arrayListOf()
        mCPosSharedViewModel.orderItemList.value = arrayListOf()

        button31.setOnClickListener(View.OnClickListener {
            showCheckoutList()
        })

        createBackgroundUpdateUiHandler()
    }

    private fun createBackgroundUpdateUiHandler() {
        @JvmStatic
        mBackgroundUpdateUiHandler = object : Handler() {
            override fun handleMessage(msg: Message?) {
                super.handleMessage(msg)
                when (msg?.what) {
                    UPDATE_UI_DATE_MESSAGE -> {
                        // update UI component here

                        var itemCount = 0
                        var totalPrice = 0.0
                        mCPosSharedViewModel.getOrderItemList()!!.forEach {
                            itemCount += it.count
                            totalPrice += (it.count * it.price)
                        }

                        tv_info_3.text = "点数：${itemCount} 点"
                        tv_info_4.text = "お買い物金額：${totalPrice} 円(税込)"
                    }
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun requestCameraPermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            android.app.AlertDialog.Builder(applicationContext)
                .setMessage("Permission Here")
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), 1)
                }
                .setNegativeButton(android.R.string.cancel) { _, _ ->
                    // TODO: can not get camera permission, need to do something
                }
                .create()
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), 1)
        }
    }

    private fun openCamera() {
        try {
            val permission = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA)
            if (permission != PackageManager.PERMISSION_GRANTED) {
                requestCameraPermission()
            } else {
                Log.i("POS", "no need to open camera here")
                // open camera here
                // 今回はただのカメラ権限を要求するから
                // カメラの起動は必要がない
            }
        } catch (e: CameraAccessException) {
            Log.e("POS", e.toString())
            e.printStackTrace()
        } catch (e: InterruptedException) {
            Log.e("POS", e.toString())
            e.printStackTrace()
        } catch (e: Exception) {
            Log.e("POS", e.toString())
            e.printStackTrace()
        }
    }

    private fun createBarcodeView() {
        mBarcodeView = findViewById<DecoratedBarcodeView>(R.id.barcode_scanner)
        val formats: Collection<BarcodeFormat> = Arrays.asList(BarcodeFormat.EAN_13)
        mBarcodeView.barcodeView.decoderFactory = DefaultDecoderFactory(formats)
        mBarcodeView.initializeFromIntent(intent)
        mBarcodeView.decodeContinuous(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                if (result!!.text == null) {
                    return
                }
                val currentTime = System.currentTimeMillis()
                if (currentTime - mBarcodeScanLastTime > BARCODE_TIME_INTERVAL) {
                    mBarcodeView.setStatusText(result.text)
                    mBeepManager.playBeepSoundAndVibrate()

                    val imageView = findViewById<ImageView>(R.id.iv_pos_right)
                    imageView.setImageBitmap(result.getBitmapWithResultPoints(Color.YELLOW))
                    Log.i("POS", "scanned barcode: ${result.text}")

                    barcodeAddItem(result.text)
                    mBarcodeScanLastTime = currentTime
                } else {
                    Log.i("POS", "ignore result: barcode scan time interval < $BARCODE_TIME_INTERVAL ms")
                }
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
            }
        })
        mBeepManager = BeepManager(this)
    }

    private fun barcodeAddItem(barcode: String) {
        if (barcode.isNotEmpty()) {
//            TODO("need to use real bar code")
            val testBarcode = "1234567890123"
            mCPosSharedViewModel.addOrderProduct(DummyProduct.getProductByBarcode(testBarcode))

            val orderFragment = supportFragmentManager.findFragmentById(R.id.order_list_container) as OrderFragment
            orderFragment.updateOrderList()

            mBackgroundUpdateUiHandler!!.sendMessage(mBackgroundUpdateUiHandler!!.obtainMessage(
                UPDATE_UI_DATE_MESSAGE))
        }
    }

    private fun showCheckoutList() {
        var listString = ""
        mCPosSharedViewModel.getOrderItemList()!!.forEach {
            listString += "ID: ${it.name} → x${it.count}\r\n"
        }

        AlertDialog.Builder(this)
            .setTitle("統計結果")
            .setMessage(listString)
            .setPositiveButton("OK", null)
            .show()
    }

    override fun onResume() {
        super.onResume()
        mBarcodeView.resume()

        val display = windowManager.defaultDisplay
        val point = Point()
        display.getSize(point)
        Log.i("PA", "display width: ${point.x} px, height: ${point.y} px")

        val metrics = resources.displayMetrics
        Log.i("PA", "display width: ${point.x / metrics.density} dp, height: ${point.y / metrics.density} dp")
    }

    override fun onPause() {
        super.onPause()
        mBarcodeView.pause()
    }

    companion object {
        private const val BARCODE_TIME_INTERVAL = 1000
        private const val UPDATE_UI_DATE_MESSAGE = 0
    }
}
