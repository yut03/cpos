package com.yut3.cpos.data.model

data class CategoryLevel(
    var level0: String,
    var level1: String,
    var level2: String,
    var level3: String,
    var level4: String,
    var level5: String,
    var level6: String
) {
    fun lastLevel(): Int {
        if (level0.isEmpty() || level0 == "0") {
            return -1
        } else if (level1.isEmpty() || level1 == "0") {
            return 0
        } else if (level2.isEmpty() || level2 == "0") {
            return 1
        }  else if (level3.isEmpty() || level3 == "0") {
            return 2
        }  else if (level4.isEmpty() || level4 == "0") {
            return 3
        }  else if (level5.isEmpty() || level5 == "0") {
            return 4
        }  else if (level6.isEmpty() || level6 == "0") {
            return 5
        } else {
            return 6
        }
    }

    fun getPreviousLevel(): CategoryLevel {
        if (lastLevel() == 6) {
            return CategoryLevel(level0, level1, level2, level3, level4, level5, "")
        } else if (lastLevel() == 5) {
            return CategoryLevel(level0, level1, level2, level3, level4, "", "")
        } else if (lastLevel() == 4) {
            return CategoryLevel(level0, level1, level2, level3, "", "", "")
        } else if (lastLevel() == 3) {
            return CategoryLevel(level0, level1, level2, "", "", "", "")
        } else if (lastLevel() == 2) {
            return CategoryLevel(level0, level1, "", "", "", "", "")
        } else if (lastLevel() == 1) {
            return CategoryLevel(level0, "", "", "", "", "", "")
        } else if (lastLevel() == 0) {
            return CategoryLevel("", "", "", "", "", "", "")
        }
        return CategoryLevel("", "", "", "", "", "", "")
    }
}